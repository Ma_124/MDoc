# -*- coding: utf-8 -*-
import random
import string
import sys

__all__ = ['error', 'warn', 'html_compat', 'international_to_ascii', 'scheme', 'obfuscate', 'get_embeddable_type',
           'Text', 'GL_Snippet', 'BB_Snippet', 'Gist', 'Pastebin', 'Image', 'Imgur', 'Audio', 'Video', 'YouTube',
           'Vimeo', 'Gfycat']


def error(self, msg, cond=False, obj=None, fobj=None):
    if cond:
        return obj
    else:
        self.log.error(msg)
        if self.abort_onerr:
            if self.abort_with_exception:
                raise SyntaxError()
            else:
                sys.exit(1)
        else:
            return fobj


def warn(self, msg, cond=False, obj=None, fobj=None):
    if cond:
        return obj
    else:
        self.log.warning(msg)
        if self.abort_onwarn:
            if self.abort_with_exception:
                raise SyntaxWarning()
            else:
                sys.exit(2)
        else:
            return fobj


def html_compat(text):
    s = ''
    prevdash = False
    for c in text:
        if c in string.ascii_lowercase:
            s += c
            prevdash = False
        elif c in string.ascii_uppercase:
            s += c.lower()
            prevdash = False
        elif c in ' ,./\\-_=':
            if not prevdash:
                s += '-'
                prevdash = True
        elif ord(c) >= 128:
            s += international_to_ascii(c)
            prevdash = False

    if prevdash:
        return s[:-1]
    return s


def international_to_ascii(c):
    # TODO don't hardcode this stuff
    if c in u'àåáâäãåą':
        return 'a'
    if c in u'èéêëę':
        return 'e'
    if c in u'ìíîïı':
        return 'i'
    if c in u'òóôõöøőð':
        return 'o'
    if c in u'ùúûüŭů':
        return 'u'
    if c in u'çćčĉ':
        return 'c'
    if c in u'żźž':
        return 'z'
    if c in u'śşšŝ':
        return 's'
    if c in u'ñń':
        return 'n'
    if c in u'ýÿ':
        return 'y'
    if c in u'ğĝ':
        return 'g'
    if c in u'ř':
        return 'r'
    if c in u'ł':
        return 'l'
    if c in u'đ':
        return 'd'
    if c in u'ĥ':
        return 'h'
    if c in u'ĵ':
        return 'j'
    if c == u'æ':
        return 'ae'
    if c == u'œ':
        return 'oe'
    if c == u'ß':
        return 'ss'
    if c == u'Þ':
        return 'th'
    return ''


def scheme(self, olink, mail=False):
    scheme = olink[:olink.index(':')].lower()
    link = olink[olink.index(':') + 1:].lstrip('/')

    if scheme == 'http' or scheme == 'https':
        return scheme + '://' + link, link, link

    if scheme == 'mailto' or mail:
        return obfuscate('mailto:' + link), obfuscate(link), obfuscate(link)  # pragma: no cover

    if scheme == 'tel':
        return scheme + ':' + link, 'Call ' + link, link

    if scheme == 'ftp':
        return scheme + '://' + link, link, link

    if scheme == 'rfc':
        return 'https://tools.ietf.org/html/rfc' + link, 'RFC ' + link, 'RFC ' + link

    # TODO github, gitlab, bitbucket (+ embeddable?)

    if scheme == 'file':
        warn(self, 'Warning: file:/// scheme')
        # return scheme + ':///' + link, None, None
        return olink, 'Local file: ' + link, link  # file:///home == file:/home != file://home

    if scheme == 'javascript':
        return error(self, 'No JS links allowed!', self.allow_js, 'javascript:' + link, fobj=''), '', ''

    if scheme == 'vbscript':
        return error(self, 'No VB links allowed!', self.allow_vb, 'vbscript:' + link, fobj=''), '', ''

    return error(self, 'Error: Unknown url scheme: ' + scheme, self.allow_unknown_schemes, obj=olink, fobj='\u00AF\_(\u30C4)_/\u00AF'), link, link


def obfuscate(s):  # pragma: no cover
    out = ''
    for c in s:
        i = random.randint(0, 2)
        if i == 0:
            out += '&#' + str(ord(c)) + ';'
        elif i == 1:
            out += '&#' + hex(ord(c))[1:] + ';'
        elif i == 2:
            out += c
    return out


Text = 10
GL_Snippet = 11
BB_Snippet = 12
Gist = 13
Pastebin = 14

# TODO messaging (twitter etc.)

Image = 20
Imgur = 21

Audio = 30

Video = 40
YouTube = 41
Vimeo = 42
Gfycat = 43  # Technically GIF € Image


def get_embeddable_type(src):
    src = str(src)

    if src.startswith('https://gitlab.com/snippets/'):
        return GL_Snippet, src[28:]  # Can you link to specific files?
    if src.startswith('http://gitlab.com/snippets/'):
        return GL_Snippet, src[27:]
    if src.startswith('snip:'):
        return GL_Snippet, src[5:].rstrip('/')
    if src.startswith('glsnip:'):
        return GL_Snippet, src[7:].rstrip('/')

    if src.startswith('https://bitbucket.org/snippets/'):
        return BB_Snippet, src[31:].split('#', 2)
    if src.startswith('http://bitbucket.org/snippets/'):
        return BB_Snippet, src[30:].split('#', 2)
    if src.startswith('bbsnip:'):
        return BB_Snippet, src[7:].rstrip('/').split('#', 2)

    if src.startswith('https://imgur.com/'):
        return Imgur, src[18:]
    if src.startswith('http://imgur.com/'):
        return Imgur, src[17:]

    if src.startswith('https://gist.github.com/'):
        return Gist, src[24:].split('#', 2)
    if src.startswith('http://gist.github.com/'):
        return Gist, src[23:].split('#', 2)
    if src.startswith('gist:'):
        return Gist, src[5:].rstrip('/').split('#', 2)

    if src.startswith('https://pastebin.com/'):
        return Pastebin, src[21:]
    if src.startswith('https://pastebin.com/'):
        return Pastebin, src[20:]
    if src.startswith('paste:'):
        return Pastebin, src[6:]

    # TODO YT urls
    if src.startswith('yt:'):
        return YouTube, src[3:].rstrip('/').split('#', 2)

    if src.startswith('https://vimeo.com/'):
        return Vimeo, src[18:]
    if src.startswith('http://vimeo.com/'):
        return Vimeo, src[17:]
    if src.startswith('vimeo:'):
        return Vimeo, src[6:].rstrip('/')

    if src.startswith('https://gfycat.com/gifs/detail/'):
        return Gfycat, src[31:]
    if src.startswith('http://gfycat.com/gifs/detail/'):
        return Gfycat, src[30:]
    if src.startswith('gfy:'):
        return Gfycat, src[4:].rstrip('/')
