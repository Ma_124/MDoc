# -*- coding: utf-8 -*-
import pytest
from ..renderer.html import HtmlRenderer
import logging
from . import *


def test_scheme():
    r = HtmlRenderer(abort_onwarn=True, abort_onerr=True, abort_with_exception=True)
    r.log.setLevel(logging.FATAL + 1)

    assert scheme(r, 'https:///www.google.de/search?source=hp') == ('https://www.google.de/search?source=hp', 'www.google.de/search?source=hp', 'www.google.de/search?source=hp')
    assert scheme(r, 'http://localhost:8080') == ('http://localhost:8080', 'localhost:8080', 'localhost:8080')
    assert scheme(r, 'tel:202-555-0128') == ('tel:202-555-0128', 'Call 202-555-0128', '202-555-0128')
    assert scheme(r, 'ftp://ftp.gnu.org/gnu/gcc/gcc-8.1.0') == ('ftp://ftp.gnu.org/gnu/gcc/gcc-8.1.0', 'ftp.gnu.org/gnu/gcc/gcc-8.1.0', 'ftp.gnu.org/gnu/gcc/gcc-8.1.0')
    assert scheme(r, 'rfc:1925') == ('https://tools.ietf.org/html/rfc1925', 'RFC 1925', 'RFC 1925')

    # Testing Errors & Warnings

    with pytest.raises(SyntaxWarning):
        scheme(r, 'file:')

    with pytest.raises(SyntaxError):
        scheme(r, 'javascript:alert("Jay I can execute a crappy language!")')

    with pytest.raises(SyntaxError):
        scheme(r, 'vbscript:MsgBox("Jay I can execute a even crappier language!", "Ok", "NOW I miss JS!")')

    with pytest.raises(SyntaxError):
        scheme(r, 'invalid://I dont care')

    # Testing special options

    r.allow_unknown_schemes = True
    assert scheme(r, 'invalid://I dont care') == ('invalid://I dont care', 'I dont care', 'I dont care')
    r.allow_unknown_schemes = False

    # Testing sys.exit as result of Errors & Warnings

    r.abort_with_exception = False

    with pytest.raises(SystemExit) as e:
        scheme(r, 'file:')

    assert e.value.code == 2

    with pytest.raises(SystemExit) as e:
        scheme(r, 'javascript:alert("Jay I can execute a crappy language!")')

    assert e.value.code == 1

    # Testing functions \w Errors & Warnings

    r.abort_onwarn = False
    r.abort_onerr = False

    assert scheme(r, 'file:////test.txt') == ('file:////test.txt', 'Local file: test.txt', 'test.txt')
    assert scheme(r, 'javascript:alert("Jay I can execute a crappy language!")') == ('', '', '')
    assert scheme(r, 'vbscript:MsgBox("Jay I can execute a even crappier language!", "Ok", "NOW I miss JS!")') == ('', '', '')


def test_html_compat():
    assert html_compat('a.b- c') == 'a-b-c'


def test_warn():
    assert warn(HtmlRenderer(), 'testing log message', True, '!') == '!'


def test_international_to_ascii():
    s = ''
    for c in u'ąïëõüčźşńýğřłđĥĵæœßÞ\u0000':
        s += international_to_ascii(c)
    assert s == 'aieouczsnygrldhjaeoessth'
