# -*- coding: utf-8 -*-
from .html import HtmlRenderer

gr = HtmlRenderer(abort_onwarn=True, abort_onerr=True, abort_with_exception=True)


def test_block_code():
    assert gr.block_code('print "Foo"\nprint "Bar"\n\n\n') == '<pre class="mdoc-code-pre"><code class="mdoc-code">print "Foo"\nprint "Bar"\n</code></pre>\n'


def test_block_quote():
    assert gr.block_quote('Some\nquoted\ntext\n\n') == '<blockquote class="mdoc-bq">Some\nquoted\ntext\n</blockquote>\n'


def test_link():
    assert gr.link('https:///google.com', 'Google', 'Text') == '<a class="mdoc-link" href="https://google.com" title="Google">Text</a>'


def test_autolink():
    assert gr.autolink('tel:202-555-0128') == '<a class="mdoc-link mdoc-autolink" href="tel:202-555-0128" title="Call 202-555-0128">202-555-0128</a>'


def test_header():
    assert gr.header(u'TeßÞ  ĥêáđíñğ ', 6, '?') == u'<h6 class="mdoc-header" id="tessth-heading">TeßÞ  ĥêáđíñğ </h6>\n'


def test_hrule():
    # #WhatWeDoForCoverage
    assert gr.hrule() == '\n<hr class="mdoc-hrule" />\n'


def test_list():
    assert gr.list('<somehtml />', ordered=True) == '<ol class="mdoc-ol"><somehtml /></ol>\n'
    assert gr.list('<somehtml />', ordered=False) == '<ul class="mdoc-ul"><somehtml /></ul>\n'


def test_list_item():
    assert gr.list_item('STUFF') == '<li class="mdoc-li">STUFF</li>\n'


def test_paragraph():
    assert gr.paragraph('TEXT') == '<p class="mdoc-p">TEXT</p>\n'


def test_double_em():
    assert gr.double_emphasis('REALLY IMPORTANT') == '<strong class="mdoc-dem">REALLY IMPORTANT</strong>'


def test_em():
    assert gr.emphasis('foo bar') == '<em class="mdoc-em">foo bar</em>'


def test_codespan():
    assert gr.codespan('print "Hello MDoc!"') == '<code class="mdoc-code-span">print "Hello MDoc!"</code>'


def test_linebreak():
    # Another round of #WhatWeDoForCoverage
    assert gr.linebreak() == '<br class="mdoc-br" />\n'


def test_strikethrough():
    assert gr.strikethrough('it doesnt use the del elem') == '<del class="mdoc-st">it doesnt use the del elem</del>'


def test_text():
    assert gr.text('<tag>&uuml;&&;') == '&lt;tag&gt;&uuml;&amp;&amp;;'


# TODO test _hide() and mailto: (maybe mock random.randint() or reverse the function (reversing code it self could be buggy))
# TODO test pygments (necessarry?)
