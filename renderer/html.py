# -*- coding: utf-8 -*-
import mistune
import logging
import uuid
import pygments.formatters.html as pyghtml
import pygments.lexers
from ..util import *


class HtmlRenderer(mistune.Renderer):
    def __init__(self, allow_unknown_schemes=False, allow_js=False, allow_vb=False, abort_onerr=True, abort_onwarn=False, abort_with_exception=False):
        super(HtmlRenderer, self).__init__()

        self.allow_unknown_schemes = allow_unknown_schemes
        self.allow_js = allow_js
        self.allow_vb = allow_vb

        self.abort_onerr = abort_onerr
        self.abort_onwarn = abort_onwarn
        self.abort_with_exception = abort_with_exception

        self.log = logging.getLogger('mdoc.HtmlRenderer.' + uuid.uuid4().hex)
        self.log.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # fh = logging.FileHandler('mdoc.renderer.log')
        # fh.setFormatter(formatter)
        # self.log.addHandler(fh)

        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        self.log.addHandler(ch)

    def block_code(self, code, lang=None):
        code = code.rstrip('\n')
        if lang is not None:
            return pygments.highlight(code, pygments.lexers.get_lexer_by_name(lang), pyghtml.HtmlFormatter(cssclass='mdoc-code-pygments'))  # pragma: no cover
        else:
            return '<pre class="mdoc-code-pre"><code class="mdoc-code">%s\n</code></pre>\n' % self.escape(code)

    def block_quote(self, text):
        return '<blockquote class="mdoc-bq">%s\n</blockquote>\n' % text.rstrip('\n')

    def header(self, text, level, raw=None):
        return '<h%d class="mdoc-header" id="%s">%s</h%d>\n' % (level, html_compat(text), text, level)

    def hrule(self):
        return '\n<hr class="mdoc-hrule" />\n'

    def list(self, body, ordered=True):
        return '<%sl class="mdoc-%sl">%s</%sl>\n' % ('o' if ordered else 'u', 'o' if ordered else 'u', body, 'o' if ordered else 'u')

    def list_item(self, text):
        return '<li class="mdoc-li">%s</li>\n' % text

    def paragraph(self, text):
        return '<p class="mdoc-p">%s</p>\n' % text

    def double_emphasis(self, text):
        return '<strong class="mdoc-dem">%s</strong>' % text

    def emphasis(self, text):
        return '<em class="mdoc-em">%s</em>' % text

    def codespan(self, text):
        text = mistune.escape(text.rstrip(), smart_amp=False)
        return '<code class="mdoc-code-span">%s</code>' % text

    def linebreak(self):
        return '<br class="mdoc-br" />\n'

    def strikethrough(self, text):
        return '<del class="mdoc-st">%s</del>' % text

    def text(self, text):
        return self.escape(text)

    def link(self, link, title, text, is_email=False, auto=False):
        link, sti, ste = scheme(self, link)

        if title is None or title == '':
            title = sti

        if text is None or text == '':
            text = ste

        return '<a class="mdoc-link%s" href="%s" title="%s">%s</a>' %\
               (' mdoc-autolink' if auto else "", self.escape(link), self.escape(title), text)

    def autolink(self, link, is_email=False):
        return self.link(link, '', '', auto=True)

    def image(self, src, title, text):
        t, data = get_embeddable_type(src)
        if t == GL_Snippet:
            return '<script src="https://gitlab.com/snippets/%s.js"></script>' % data
        if t == Image:
            return '<img class="mdoc-img" title="%s" alt="%s" src="%s" />' % title, text, src
        if t == Audio:
            return '<audio class="mdoc-audio" title="%s" src="%s" />' % title, src
        if t == Video:
            return '<video class="mdoc-vid" controls title="%s" src="%s" />' % title, src
        if t == YouTube:
            return ('<iframe class="mdoc-yt" width="1871" height="790" src="https://www.youtube.com/embed/%s"' +
                    'frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>') % data
        if t == Gfycat:
            return ("<div style='position:relative;padding-bottom:54%'><iframe src='https://gfycat.com/ifr/%s' " +
                    "frameborder='0' scrolling='no' width='100%' height='100%' " +
                    "style='position:absolute;top:0;left:0' allowfullscreen></iframe></div><p> " +
                    "<a href='https://gfycat.com/gifs/detail/%s'>via Gfycat</a></p>") % data, data
        return error(self, 'Unsupported source: ' + src, fobj='')
